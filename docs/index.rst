Atooms
======

**atooms** is a Python framework for simulations of interacting particles. It makes it easy to develop simulation and analysis tools using an expressive language, without sacrificing efficiency. To achieve this, atooms relies on backends written in C, CUDA or Fortran.

.. toctree::
   :maxdepth: 1
   :caption: API documentation
     
   api/atooms/backends/index.rst
   api/atooms/core/index.rst
   api/atooms/optimization/index.rst
   api/atooms/simulation/index.rst
   api/atooms/system/index.rst
   api/atooms/trajectory/index.rst 

   
