{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9b53338b",
   "metadata": {},
   "outputs": [],
   "source": [
    "# This allows execution from the docs/ directory\n",
    "import sys\n",
    "if '../' not in sys.path:\n",
    "    sys.path.insert(0, '../')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e58bd492",
   "metadata": {},
   "source": [
    "# Trajectories\n",
    "\n",
    "## Trajectory formats\n",
    "\n",
    "`atooms`{.verbatim} supports several trajectory formats, most of them in\n",
    "read and write mode\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "cc3ccf3e",
   "metadata": {},
   "outputs": [],
   "source": [
    "from atooms.trajectory import formats\n",
    "\n",
    "print(formats())"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "deef68fa",
   "metadata": {},
   "source": [
    "```\n",
    "available trajectory formats:\n",
    " - csv          : [RW] ...no description...\n",
    " - dynamo       : [R ] DynamO trajectory format (https://www.dynamomd.com/index.php/tutorial3)\n",
    " - exyz         : [RW] Extended XYZ layout (https://github.com/libAtoms/extxyz)\n",
    " - folderlammps : [R ] Multi-file layout LAMMPS format.\n",
    " - gsd          : [RW] Glotzer group's binary GSD format for HOOMD (https://glotzerlab.engin.umich.edu/hoomd-blue/)\n",
    " - hdf5         : [RW] In-house trajectory layout in HDF5 format.\n",
    " - hoomd        : [RW] HOOMD format\n",
    " - lammps       : [RW] LAMMPS format (https://docs.lammps.org/dump.html)\n",
    " - neighbors    : [RW] Neighbors trajectory format\n",
    " - pdb          : [RW] PDB format (https://en.wikipedia.org/wiki/Protein_Data_Bank_(file_format))\n",
    " - ram          : [RW] Store trajectory in RAM\n",
    " - rumd         : [RW] RUMD trajectory format (https://rumd.org)\n",
    " - simplexyz    : [RW] Simple implementation of the xyz layout (https://en.wikipedia.org/wiki/XYZ_file_format)\n",
    " - superrumd    : [R ] SuperTrajectory for RUMD format\n",
    " - xyz          : [RW] XYZ format with metadata support (https://en.wikipedia.org/wiki/XYZ_file_format)\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "94ef991c",
   "metadata": {},
   "source": [
    "## Custom trajectory formats\n",
    "\n",
    "It is easy to add new trajectory formats by subclassing existing\n",
    "trajectory classes. To make these new classes accessible also to\n",
    "`trj.py`{.verbatim}, create a package called `atooms_plugins`{.verbatim}\n",
    "and add your trajectory modules there. Suppose you wrote a custom\n",
    "trajectory class `TrajectoryABC`{.verbatim} in\n",
    "`atooms_plugins/test.py`{.verbatim} (the last path is relative to the\n",
    "current directory). You can now convert an existing xyz trajectory to\n",
    "your custom format:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8bd330d2",
   "metadata": {},
   "outputs": [],
   "source": [
    "! trj.py convert output.xyz output.abc"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "945f8066",
   "metadata": {},
   "source": [
    "Remember to add an empty `__init__.py`{.verbatim} file at the root of\n",
    "`atooms_plugins`{.verbatim}. Actually, the `atooms_plugins`{.verbatim}\n",
    "package can be put anywhere in your `PYTHONPATH`{.verbatim}.\n",
    "\n",
    "## Custom trajectory output\n",
    "\n",
    "We can customize the format of trajectory files using the\n",
    "`fields`{.verbatim} variable. It contains a list of the particle\n",
    "properties to be written to the trajectory. For this simple example we\n",
    "use again the xyz trajectory format.\n",
    "\n",
    "We add a `charge`{.verbatim} property to each particle and then instruct\n",
    "the trajectory to write it along with the position\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b2eb67e8",
   "metadata": {},
   "outputs": [],
   "source": [
    "from atooms.system import System, Cell, Particle\n",
    "system = System(particle=[Particle() for i in range(3)],\n",
    "\t\tcell=Cell([10.0, 10.0, 10.0]))\n",
    "\n",
    "for p in system.particle:\n",
    "    p.charge = -1.0\n",
    "\n",
    "with TrajectoryXYZ('test.xyz', 'w', fields=['position', 'charge']) as th:\n",
    "    th.write(system, step=0)\n",
    "\n",
    "with open('test.xyz') as fh:\n",
    "    print(fh.read())"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "127b48d3",
   "metadata": {},
   "source": [
    "The `fields`{.verbatim} list can contain any particle property, even\n",
    "those defined dynamically at run time, such as the `charge`{.verbatim}\n",
    "variable above which is not a predefined particle property!. When\n",
    "reading back the trajectory, the `charge`{.verbatim} property is\n",
    "automatically recognized and added to the particle.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "292f250a",
   "metadata": {},
   "outputs": [],
   "source": [
    "with TrajectoryXYZ('test.xyz') as th:\n",
    "  system = th[0]\n",
    "  print(system.particle[0].charge)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1ffadf37",
   "metadata": {},
   "source": [
    "## Conversion between trajectory formats\n",
    "\n",
    "Atooms provides means to convert between trajectory various formats. At\n",
    "a very basic level, this requires opening the original trajectory for\n",
    "reading and the new one for writing using the desired trajectory class.\n",
    "Here we convert an xyz trajectory in a format suitable for the LAMMPS\n",
    "package\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a2cef584",
   "metadata": {},
   "outputs": [],
   "source": [
    "from atooms.trajectory import TrajectoryLAMMPS\n",
    "with TrajectoryXYZ('test.xyz') as th_inp,\\\n",
    "     TrajectoryLAMMPS('test.lammps', 'w') as th_out:\n",
    "    for i, system in enumerate(th_inp):\n",
    "        th_out.write(system, th_inp.steps[i])"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "40087b41",
   "metadata": {},
   "source": [
    "The `convert()`{.verbatim} function wraps the conversion in a more\n",
    "convenient interface\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4781c06b",
   "metadata": {},
   "outputs": [],
   "source": [
    "from atooms.trajectory import convert\n",
    "convert(TrajectoryXYZ('test.xyz'), TrajectoryLAMMPS, 'test.lammps')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f759bc34",
   "metadata": {},
   "source": [
    "There are several optional parameters that allows to customize the\n",
    "trajectory output, see the function signature for more details.\n",
    "\n",
    "Finally, the `trj.py`{.verbatim} script installed by atooms allows to\n",
    "quickly convert trajectories on the command-line, which is actually the\n",
    "most frequent use case\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "eb8f8c90",
   "metadata": {},
   "outputs": [],
   "source": [
    "! trj.py convert -i xyz -o lammps test.xyz test.lammps"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "cdfd19e6",
   "metadata": {},
   "source": [
    "Although the script will do its best to guess the appropriate trajectory\n",
    "formats, it is best to provide the input and output trajectory formats\n",
    "via the `-i`{.verbatim} and `-o`{.verbatim} flags explicitly.\n",
    "\n",
    "## Add and modify trajectory properties on the fly with callbacks\n",
    "\n",
    "\\\"Callbacks\\\" are functions used to modify the properties of a\n",
    "trajectory on the fly. They accept a `System`{.verbatim} instance as\n",
    "first positional argument, along with optional extra positional and\n",
    "keyword arguments, and return a modified `System`{.verbatim}.\n",
    "\n",
    "As an example, suppose your trajectory did not provide any information\n",
    "about the cell side. You can add the information dynamically to all\n",
    "`System`{.verbatim} objects read from the trajectory using the following\n",
    "callback\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a3a2cd12",
   "metadata": {},
   "outputs": [],
   "source": [
    "from atooms.system import Cell\n",
    "def fix_missing_cell(system, side):\n",
    "    system.cell = Cell(side)\n",
    "    return system"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b01b343a",
   "metadata": {},
   "source": [
    "Then we add the callback to the trajectory and provide the cell side\n",
    "(here L=10 along each dimensions) as argument. Reading the trajectory is\n",
    "then done as usual.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "af523a9d",
   "metadata": {},
   "outputs": [],
   "source": [
    "from atooms.trajectory import TrajectoryXYZ\n",
    "with TrajectoryXYZ('test.xyz') as th:\n",
    "    th.add_callback(fix_missing_cell, [10., 10., 10.])\n",
    "    for system in th:\n",
    "        print(system.cell.side)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d6d8347f",
   "metadata": {},
   "source": [
    "## Extend trajectory classes\n",
    "\n",
    "Suppose you have a trajectory that looks almost like xyz, but differs in\n",
    "some way. You may want to customize the xyz trajectory format, so that\n",
    "your code can process the trajectory without manual intervention.\n",
    "\n",
    "For instance, your xyz file is `test.xyz`{.verbatim} but the cell side\n",
    "information is stored in a separate file `test.xyz.cell`{.verbatim}. We\n",
    "can proceed as before\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6a8e58a0",
   "metadata": {},
   "outputs": [],
   "source": [
    "from atooms.system import Cell\n",
    "\n",
    "file_inp = 'test.xyz'\n",
    "with open(file_inp + '.cell') as fh:\n",
    "    # Assume the cell file contains a string Lx Ly Lz\n",
    "    # where Lx, Ly, Lz are the sides of the orthorombic cell\n",
    "    side = [float(L) for L in fh.read().split()]\n",
    "\n",
    "with TrajectoryXYZ(file_inp) as th:\n",
    "    th.add_callback(fix_missing_cell, side)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4ec13358",
   "metadata": {},
   "source": [
    "As a more permanent solution, you can define your own custom trajectory\n",
    "by subclassing `TrajectoryXYZ`{.verbatim}. First, parse the cell\n",
    "information during the initialization stage (`read_init()`{.verbatim}).\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8a5710f8",
   "metadata": {},
   "outputs": [],
   "source": [
    "from atooms.system import Cell\n",
    "from atooms.trajectory import TrajectoryXYZ\n",
    "\n",
    "class TrajectoryCustomXYZ(TrajectoryXYZ):\n",
    "\n",
    "    def read_init(self):\n",
    "        super().read_init()\n",
    "        with open(self.filename + '.cell') as fh:\n",
    "            self._side = [float(L) for L in fh.read().split()]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "fd7d6f2b",
   "metadata": {},
   "source": [
    "Then modify the `read_sample()`{.verbatim} method, which reads a given\n",
    "frame of the trajectory.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a0bcdff4",
   "metadata": {},
   "outputs": [],
   "source": [
    "    def read_sample(self, frame):\n",
    "        system = super().read_sample()\n",
    "        system.cell = Cell(self._side)\n",
    "        return system"
   ]
  }
 ],
 "metadata": {},
 "nbformat": 4,
 "nbformat_minor": 5
}
