{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ebcbb78f",
   "metadata": {},
   "outputs": [],
   "source": [
    "# This allows execution from the docs/ directory\n",
    "import sys\n",
    "if '../' not in sys.path:\n",
    "    sys.path.insert(0, '../')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "1f189c7b",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Internal setup\n",
    "import matplotlib.pyplot as plt\n",
    "plt.rcParams.update({\n",
    "    \"font.family\": \"serif\",\n",
    "    'font.size': 11.0,\n",
    "    'axes.labelsize': 'medium',\n",
    "    'xtick.major.pad': 2.0,\n",
    "    'ytick.major.pad': 2.0,\n",
    "    'xtick.major.size': 4.0,\n",
    "    'ytick.major.size': 4.0,\n",
    "    'savefig.bbox': 'tight',\n",
    "    'savefig.dpi': 180,\n",
    "    'axes.spines.right': False,\n",
    "    'axes.spines.top': False,\n",
    "    'legend.frameon': False,\n",
    "})"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "82de603b",
   "metadata": {},
   "source": [
    "# Simulations\n",
    "\n",
    "The atooms\\' interface abstracts out most of the common tasks of\n",
    "particle-based simulations. The actual simulation is performed by a\n",
    "simulation \\\"backend\\\", which exposes a minimal but consistent\n",
    "interface. This enables one to develop complex simulation frameworks\n",
    "(e.g., [parallel\n",
    "tempering](https://framagit.org/atooms/parallel_tempering)) that are\n",
    "essentially decoupled from the underlying simulation code.\n",
    "\n",
    "A **Simulation** is a high-level class that encapsulates some common\n",
    "tasks, like regularly storing data on files, and provides a consistent\n",
    "interface to the user, while **backend** classes actually make the\n",
    "system evolve. Here, we implement a minimal backend to run a simulation.\n",
    "\n",
    "At a very minimum, a backend is a class that provides\n",
    "\n",
    "-   a **system** instance variable, which should (mostly) behave like\n",
    "    `atooms.system.System`{.verbatim}\n",
    "-   a **run()** method, which evolves the system for a prescribed number\n",
    "    of steps (passed as argument)\n",
    "\n",
    "Optionally, the backend may hold a reference to a trajectory class,\n",
    "which can be used to checkpoint the simulation or to write\n",
    "configurations to a file. This is however not required in a first stage.\n",
    "\n",
    "## A minimal simulation backend\n",
    "\n",
    "We set up a bare-bones simulation backend building on the native System\n",
    "class\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "24e9ebbb",
   "metadata": {},
   "outputs": [],
   "source": [
    "from atooms.system import System\n",
    "\n",
    "class BareBonesBackend(object):\n",
    "    \n",
    "    def __init__(self):\n",
    "        self.system = System()\n",
    "\n",
    "    def run(self, steps):\n",
    "        for i in range(steps):\n",
    "            pass\n",
    "\n",
    "# The backend is created and wrapped by a simulation object.\n",
    "# Here we first call the run() method then run_until()\n",
    "from atooms.simulation import Simulation\n",
    "from atooms.simulation.core import _log as logger\n",
    "backend = BareBonesBackend()\n",
    "simulation = Simulation(backend)\n",
    "simulation.run(10)\n",
    "simulation.run_until(30)\n",
    "assert simulation.current_step == 30\n",
    "\n",
    "# This time we call run() multiple times \n",
    "simulation = Simulation(backend)\n",
    "simulation.run(10)\n",
    "simulation.run(20)\n",
    "assert simulation.current_step == 30  \n",
    "\n",
    "# Set up verbose logging to see a meaningful log\n",
    "from atooms.core.utils import setup_logging\n",
    "setup_logging(level=20, update=True)\n",
    "simulation = Simulation(backend)\n",
    "simulation.run(10)\n",
    "setup_logging(level=40, update=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e600265b",
   "metadata": {},
   "source": [
    "```\n",
    "# \n",
    "# atooms simulation via <__main__.BareBonesBackend object at 0x7f6f4f8e1430>\n",
    "# \n",
    "# version: 3.14.1+3.14.1-7-g7ec3c6-dirty (2022-12-10)\n",
    "# atooms version: 3.14.1+3.14.1-7-g7ec3c6-dirty (2022-12-10)\n",
    "# simulation started on: 2022-12-21 at 11:04\n",
    "# output path: None\n",
    "# backend: <class '__main__.BareBonesBackend'>\n",
    "# \n",
    "# target target_steps: 10\n",
    "# \n",
    "# \n",
    "# <__main__.BareBonesBackend object at 0x7f6f4f8e1430>\n",
    "# simulation ended successfully: reached target steps 10\n",
    "# \n",
    "# final steps: 10\n",
    "# final rmsd: 0.00\n",
    "# wall time [s]: 0.00\n",
    "# average TSP [s/step/particle]: nan\n",
    "# simulation ended on: 2022-12-21 at 11:04\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "60d9000c",
   "metadata": {},
   "source": [
    "## Simple random walk\n",
    "\n",
    "We implement a simple random walk in 3d. This requires adding code to\n",
    "the backend `run()`{.verbatim} method to actually move the particles\n",
    "around. The code won\\'t be very fast! See below [*how to implement a\n",
    "backend efficiently*]{.spurious-link target=\"*Faster backends\"}.\n",
    "\n",
    "We start by building an empty system. Then we add a few particles and\n",
    "place them at random in a cube. Finally, we write a backend that\n",
    "displaces each particle randomly over a cube of prescribed side.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "7a718a44",
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy\n",
    "from random import random\n",
    "from atooms.system import System\n",
    "from atooms.system.particle import Particle\n",
    "\n",
    "system = System()\n",
    "L = 10\n",
    "for i in range(1000):\n",
    "    p = Particle(position=[L * random(), L * random(), L * random()])\n",
    "    system.particle.append(p)\n",
    "\n",
    "class RandomWalk(object):\n",
    "\n",
    "    def __init__(self, system, delta=1.0):\n",
    "        self.system = system\n",
    "        self.delta = delta\n",
    "\n",
    "    def run(self, steps):\n",
    "        for i in range(steps):\n",
    "            for p in self.system.particle:\n",
    "                dr = numpy.array([random()-0.5, random()-0.5, random()-0.5])\n",
    "                dr *= self.delta\n",
    "                p.position += dr"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9eab59f8",
   "metadata": {},
   "source": [
    "## Adding callbacks\n",
    "\n",
    "The Simulation class allows you to execute of arbitrary code during the\n",
    "simulation via \\\"callbacks\\\". They can be used for instance to\n",
    "\n",
    "-   store simulation data\n",
    "-   write logs or particle configurations to trajectory files\n",
    "-   perform on-the-fly calculations of the system properties\n",
    "-   define custom conditions to stop the simulation\n",
    "\n",
    "Callbacks are plain function that accept the simulation object as first\n",
    "argument. They are called at prescribed intervals during the simulation.\n",
    "\n",
    "As an example, we measure the mean square displacement (MSD) of the\n",
    "particles to make sure that the system displays a regular diffusive\n",
    "behavior $MSD \\sim t$\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "14b00aa4",
   "metadata": {},
   "outputs": [],
   "source": [
    "from atooms.simulation import Simulation\n",
    "simulation = Simulation(RandomWalk(system))\n",
    "\n",
    "# We add a callback that computes the MSD every 10 steps\n",
    "# We store the result in a dictionary passed to the callback\n",
    "msd_db = {}\n",
    "def cbk(sim, initial_position, db):\n",
    "    msd = 0.0\n",
    "    for i, p in enumerate(sim.system.particle):\n",
    "        dr = p.position - initial_position[i]\n",
    "        msd += numpy.sum(dr**2)\n",
    "    msd /= len(sim.system.particle)\n",
    "    db[sim.current_step] = msd\n",
    "\n",
    "# We will execute the callback every 10 steps\n",
    "simulation.add(cbk, 10, initial_position=[p.position.copy() for p in\n",
    "                                          system.particle], db=msd_db)\n",
    "simulation.run(50)\n",
    "\n",
    "# The MSD should increase linearly with time\n",
    "time = sorted(msd_db.keys())\n",
    "msd = [msd_db[t] for t in time]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c1bbb7e7",
   "metadata": {},
   "source": [
    "The MSD as a function of time should look linear.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d36b2a81",
   "metadata": {},
   "outputs": [],
   "source": [
    "import matplotlib.pyplot as plt\n",
    "plt.plot(time, msd, '-o')\n",
    "plt.xlabel(\"t\")\n",
    "plt.ylabel(\"MSD\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "117abb95",
   "metadata": {},
   "source": [
    "![](msd.png)\n",
    "\n",
    "The\n",
    "[postprocessing](https://gitlab.info-ufr.univ-montp2.fr/atooms/postprocessing/)\n",
    "component package provides way more options to compute dynamic\n",
    "correlation functions.\n",
    "\n",
    "## Fine-tuning the scheduler\n",
    "\n",
    "Calling a callback can be done at regular intervals during the\n",
    "simulation or according to a custom schedule defined by a\n",
    "`Scheduler`{.verbatim}. Here we consider the\n",
    "`simulation.write_trajectory()`{.verbatim} callback, which writes the\n",
    "system state in a trajectory file\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a54e5206",
   "metadata": {},
   "outputs": [],
   "source": [
    "from atooms.trajectory import TrajectoryXYZ\n",
    "from atooms.simulation import write_trajectory, Scheduler\n",
    "\n",
    "simulation = Simulation(RandomWalk(system))\n",
    "trajectory = TrajectoryXYZ('/tmp/trajectory.xyz', 'w')\n",
    "# Write every 10 steps\n",
    "simulation.add(write_trajectory, Scheduler(10), trajectory=trajectory)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "05f4be97",
   "metadata": {},
   "source": [
    "Here are a few options of the Scheduler:\n",
    "\n",
    "-   `interval`{.verbatim}: notify at a fixed steps interval (default)\n",
    "-   `calls`{.verbatim}: fixed number of calls to the callback\n",
    "-   `steps`{.verbatim}: list of steps at which the callback will be\n",
    "    called\n",
    "-   `block`{.verbatim}: as steps, but the callback will be called\n",
    "    periodically\n",
    "-   `seconds`{.verbatim}: notify every `seconds`{.verbatim}\n",
    "\n",
    "One useful application of the Scheduler is writing frames in a\n",
    "trajectory at exponentialy spaced intervals. Here the\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "dddd49e5",
   "metadata": {},
   "outputs": [],
   "source": [
    "trajectory_exp = TrajectoryXYZ('/tmp/trajectory_exp.xyz', 'w')\n",
    "simulation.add(write_trajectory, Scheduler(block=[0, 1, 2, 4, 8, 16]), trajectory=trajectory_exp)\n",
    "simulation.run(32)\n",
    "trajectory.close()\n",
    "trajectory_exp.close()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "72c14a7b",
   "metadata": {},
   "source": [
    "Now we will have two trajectories, one with regular and the other with\n",
    "exponentially spaced blocks of frames\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a9cea772",
   "metadata": {},
   "outputs": [],
   "source": [
    "with TrajectoryXYZ('/tmp/trajectory.xyz') as th, \\\n",
    "     TrajectoryXYZ('/tmp/trajectory_exp.xyz') as th_exp:\n",
    "    print('Regular:', th.steps)\n",
    "    print('Exponential:', th_exp.steps)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "16c75a08",
   "metadata": {},
   "source": [
    "## Compute statistical averages\n",
    "\n",
    "The `simulation.store()`{.verbatim} callback allows you to store data in\n",
    "a dictionary while the simulation is running. Here are a few ways to use\n",
    "it to perform some statistical analysis.\n",
    "\n",
    "The `store`{.verbatim} callback accepts an array of arguments to store.\n",
    "They can be string matching a few predefined attributes (such as\n",
    "`steps`{.verbatim}, the current number of steps carried out by the\n",
    "backend) or a general attribute of the `simulation`{.verbatim} instance\n",
    "(such as `system.particle[0].position[0]`{.verbatim}, the x-coordinate\n",
    "of the first particle of the system).\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "aa77f58c",
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy\n",
    "from atooms.simulation import store\n",
    "\n",
    "simulation = Simulation(RandomWalk(system))\n",
    "simulation.add(store, 1, ['steps', 'system.particle[0].position[0]'])"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4332792c",
   "metadata": {},
   "source": [
    "By default, after running the simulation, the data will be stored in the\n",
    "`simulation.data`{.verbatim} dictionary and you can use it for further\n",
    "analysis\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c437fa9a",
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy\n",
    "simulation.run(10)\n",
    "print(numpy.mean(simulation.data['system.particle[0].position[0]']))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d4883720",
   "metadata": {},
   "source": [
    "You can store the result of any function that takes as first argument\n",
    "the simulation instance. Just add a tuple with a label and the function\n",
    "to the list of properties to store.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8c642f73",
   "metadata": {},
   "outputs": [],
   "source": [
    "simulation = Simulation(RandomWalk(system))\n",
    "simulation.add(store, 1, ['steps', ('x_1', lambda sim: sim.system.particle[1].position[0])])\n",
    "simulation.run(10)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "781cd28d",
   "metadata": {},
   "source": [
    "## Faster backends\n",
    "\n",
    "Moving particles using the `Particle`{.verbatim} object interface is\n",
    "expressive but computationally very slow, since it forces us to operate\n",
    "one particle at a time. We can write a more efficient backend by getting\n",
    "a \\\"view\\\" of the system\\'s coordinates as a numpy array and operating\n",
    "on it vectorially. You can also pass the viewed arrays to backends\n",
    "written in compiled languages (even just in time).\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "02ee744a",
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy\n",
    "from atooms.system import System\n",
    "\n",
    "# Create a system with 10 particles\n",
    "system = System(N=10)\n",
    "\n",
    "class FastRandomWalk(object):\n",
    "\n",
    "    def __init__(self, system, delta=1.0):\n",
    "        self.system = system\n",
    "        self.delta = delta\n",
    "\n",
    "    def run(self, steps):\n",
    "        # Get a view on the particles' position\n",
    "        pos = self.system.view(\"position\")\n",
    "        for i in range(steps):\n",
    "            dr = (numpy.random(pos.shape) - 0.5) * self.delta\n",
    "            # Operate on array in-place\n",
    "            pos += dr"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "90b76a84",
   "metadata": {},
   "source": [
    "The viewed array can be cast in C-order (default) or F-order using the\n",
    "`order`{.verbatim} parameter\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a3867c03",
   "metadata": {},
   "outputs": [],
   "source": [
    "system.view(\"position\", order='C')\n",
    "system.view(\"position\", order='F')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d202e1f6",
   "metadata": {},
   "source": [
    "If $N$ is the number of particles and $d$ is the number of spatial\n",
    "dimensions, then you\\'ll get\n",
    "\n",
    "-   $(N, d)$-dimensional arrays with `order`{.verbatim}\\'C\\'= (default)\n",
    "-   $(d, N)$-dimensional arrays with `order`{.verbatim}\\'F\\'=\n",
    "\n",
    "Of course, this option is relevant only for vector attributes like\n",
    "positions and velocities.\n",
    "\n",
    "You can get a view of any system property by providing a \\\"fully\n",
    "qualified\\\" attribute\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "38e14731",
   "metadata": {},
   "outputs": [],
   "source": [
    "assert numpy.all(system.view(\"cell.side\") == system.cell.side)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "073d9925",
   "metadata": {},
   "source": [
    "In particular, for particles\\' attributes you can use this syntax\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "766adc9b",
   "metadata": {},
   "outputs": [],
   "source": [
    "assert numpy.all(system.view(\"particle.position\") == system.view(\"pos\"))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0d3e1605",
   "metadata": {},
   "source": [
    "## Molecular dynamics with LAMMPS\n",
    "\n",
    "Atooms provides a simulation backend for `LAMMPS`{.verbatim}, an\n",
    "efficient and feature-rich molecular dynamics simulation package. The\n",
    "backend accepts a string variable containing regular LAMMPS commands and\n",
    "initial configuration to start the simulation. The latter can be\n",
    "provided in any of the following forms:\n",
    "\n",
    "-   a `System`{.verbatim} object\n",
    "-   a `Trajectory`{.verbatim} object\n",
    "-   the path to an xyz trajectory\n",
    "\n",
    "In the last two cases, the last configuration will be used to start the\n",
    "simulation.\n",
    "\n",
    "Here we we use the first configuration of an existing trajectory for a\n",
    "Lennard-Jones fluid\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "df262870",
   "metadata": {},
   "outputs": [],
   "source": [
    "import os\n",
    "from atooms.core.utils import download\n",
    "import atooms.trajectory as trj\n",
    "from atooms.backends import lammps\n",
    "\n",
    "# You can change it so that it points to the LAMMPS executable\n",
    "lammps.lammps_command = 'lmp'\n",
    "\n",
    "download('https://framagit.org/atooms/atooms/raw/master/data/lj_N1000_rho1.0.xyz', \"/tmp\")\n",
    "system = trj.TrajectoryXYZ('/tmp/lj_N1000_rho1.0.xyz')[0]\n",
    "cmd = \"\"\"\n",
    "pair_style      lj/cut 2.5\n",
    "pair_coeff      1 1 1.0 1.0  2.5\n",
    "neighbor        0.3 bin\n",
    "neigh_modify    check yes\n",
    "timestep        0.002\n",
    "\"\"\"\n",
    "backend = lammps.LAMMPS(system, cmd)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "10f29522",
   "metadata": {},
   "source": [
    "We now wrap the backend in a simulation instance. This way we can rely\n",
    "on atooms to write thermodynamic data and configurations to disk during\n",
    "the simulation: we just add the `write_config()`{.verbatim} and\n",
    "`write_thermo()`{.verbatim} callbacks to the simulation. You can add\n",
    "your own functions as callbacks to perform arbitrary manipulations on\n",
    "the system during the simulation. Keep in mind that calling these\n",
    "functions causes some overhead, so avoid calling them at too short\n",
    "intervals.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6c24fbcc",
   "metadata": {},
   "outputs": [],
   "source": [
    "from atooms.simulation import Simulation\n",
    "from atooms.system import Thermostat\n",
    "from atooms.simulation.observers import store, write_config\n",
    "\n",
    "# We create the simulation instance and set the output path\n",
    "sim = Simulation(backend, output_path='/tmp/lammps.xyz')\n",
    "# Write configurations every 1000 steps in xyz format\n",
    "sim.add(write_config, 1000, trajectory_class=trj.TrajectoryXYZ)\n",
    "# Store thermodynamic properties every 500 steps\n",
    "sim.add(store, 100, ['steps', 'potential energy per particle', 'temperature'])"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "34d8cb96",
   "metadata": {},
   "source": [
    "We add a thermostat to keep the system temperature at T=2.0 and run the\n",
    "simulations for 10000 steps.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d62d67f0",
   "metadata": {},
   "outputs": [],
   "source": [
    "backend.system.thermostat = Thermostat(temperature=2.0, relaxation_time=0.1)\n",
    "sim.run(4000)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9eaadf02",
   "metadata": {},
   "source": [
    "Note that we use atooms `Thermostat`{.verbatim} object here: the backend\n",
    "will take care of adding appropriate commands to the LAMMPS script.\n",
    "\n",
    "We have a quick look at the kinetic temperature as function of time to\n",
    "make sure the thermostat is working\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "7f522531",
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.plot(sim.data['steps'], sim.data['temperature'])\n",
    "plt.xlabel('Steps')\n",
    "plt.ylabel('Temperature')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "63d77a71",
   "metadata": {},
   "source": [
    "![](lammps.png)\n",
    "\n",
    "We can then use the\n",
    "[postprocessing](https://gitlab.info-ufr.univ-montp2.fr/atooms/postprocessing/)\n",
    "package to compute the radial distribution function or any other\n",
    "correlation function from the trajectory.\n",
    "\n",
    "## Molecular dynamics with RUMD\n",
    "\n",
    "There is native support for an efficient MD molecular dynamics code\n",
    "running entirely on GPU called [RUMD](https://rumd.org), developed by\n",
    "the Glass and Time group in Roskilde. It is optimized for small and\n",
    "medium-size systems.\n",
    "\n",
    "Here we pick the last frame of the trajectory, change the density of the\n",
    "system to unity and write this new configuration to a trajectory format\n",
    "suitable for RUMD\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e35f34c6",
   "metadata": {},
   "outputs": [],
   "source": [
    "from atooms.trajectory import Trajectory\n",
    "\n",
    "with Trajectory('/tmp/lj_N1000_rho1.0.xyz') as trajectory:\n",
    "    system = trajectory[-1]\n",
    "    system.density = 1.0\n",
    "    print('New density:', round(len(system.particle) / system.cell.volume, 2))\n",
    "\n",
    "from atooms.trajectory import TrajectoryRUMD\n",
    "with TrajectoryRUMD('rescaled.xyz.gz', 'w') as trajectory:\n",
    "    trajectory.write(system)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "875c0399",
   "metadata": {},
   "source": [
    "Now we run a short molecular dynamics simulation with the\n",
    "`RUMD`{.verbatim} backend, using a Lennard-Jones potential:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "339b306e",
   "metadata": {},
   "outputs": [],
   "source": [
    "import rumd\n",
    "from atooms.backends.rumd import RUMD\n",
    "from atooms.simulation import Simulation\n",
    "\n",
    "potential = rumd.Pot_LJ_12_6(cutoff_method=rumd.ShiftedPotential)\n",
    "potential.SetParams(i=0, j=0, Epsilon=1.0, Sigma=1.0, Rcut=2.5)\n",
    "backend = RUMD('rescaled.xyz.gz', [potential], integrator='nve'\n",
    "sim = Simulation(backend)\n",
    "sim.run(1000)"
   ]
  }
 ],
 "metadata": {},
 "nbformat": 4,
 "nbformat_minor": 5
}
