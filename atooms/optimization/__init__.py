# This file is part of atooms
# Copyright 2010-2024, Daniele Coslovich

"""
Optimization framework for systems of interacting particles.

`atooms` provides a generic optimization interface that abstracts out
most of the common parts of particle-based optimizations.
"""

from .core import Optimization
