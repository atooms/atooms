# Changelog

This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html). This file only reports changes that increase major and minor versions, as well as deprecations and critical bug fixes.

## 3.24.1 - 2025/01/19

[Full diff](https://framagit.org/atooms/atooms.git/-/compare/3.24.0...3.24.1)

###  Bug fixes
- Fix bugs with `Unfolded` and `Sliced` trajectory decorators
  - Callbacks were propagated to the paren instances by the decorated one, possibly leading to nonlocal effects
  - `Sliced` did not work correctly when `cache` was enabled in the parent instance

## 3.24.0 - 2025/01/15

[Full diff](https://framagit.org/atooms/atooms/-/compare/3.23.0...3.24.0)

###  New
- Add `Molecule` class and support for molecules in `System`
- Add `create_molecular_system()` callback to `trajectory.decorators`. It creates a `System` with both `Molecule` and `Particle` objects from a standard `System` with `Particle` objects. The particles must possess an integer attribute `molecule_id` to be identified as parts of a molecule.
- Add `_Molecular` class decorator to subclass trajectories that support molecular info
- Add molecular trajectories `MolecularTrajectoryLAMMPS`, `MolecularTrajectoryXYZ` and `MolecularTrajectoryEXYZ` that create a Trajectory with both Molecule and Particle objects from a standard `Trajectory`
- Add `System.set_concentration`, also as `concentration` property setter
- Add support to underscore-separated fields in `write()` and `store()` simlation observers
- Add `spacing` attribute to `particle_lattice()` to fix the lattice spacing

###  Bug fixes
- Fix inclusion of package files by adding `MANIFEST.in`
- Fix `f90` backend with some potentials, by removing automatic determination of the number of interacting bodies
- Fix namespace package installation with pyproject.toml
- Fix `report_simulation()` when `partial_rmsd` is missing
- Fix `Particle.nearest_image_of()`
- Fix links in changelog

## 3.23.0 - 2024/12/14

[Full diff](https://framagit.org/atooms/atooms/-/compare/3.22.1...3.23.0)

###  New
- Support Python versions from 3.7 up to 3.13
- Add `[backends]` extra install 
- Add support to `C` species layout in `f90` backend
- Add `dimensions` argument to f90.Interaction
- Add `InteractionWall` and `InteractionField`
- Add `ignore_id` in `TrajectoryLAMMPS` to ignore particle indices
- Add support to binned neighbor lists in `VerletList`

###  Bug fixes
- Fix `particle._lattice()` with large `N`
- Fix `report_simulation()`
- Fix numpy deprecations
- Improve cell parsing for lammps trajectories
- Switch to `pyproject.toml`

## 3.22.1 - 2024/10/13

[Full diff](https://framagit.org/atooms/atooms/-/compare/3.22.0...3.22.1)

###  Bug fixes
- Fix numpy dependency, at present we only support numpy<2

## 3.22.0 - 2024/05/18

[Full diff](https://framagit.org/atooms/atooms/-/compare/3.21.0...3.22.0)

###  New
- Add arguments `units`, `style`, `verbose` to lammps backend

###  Bug fixes
- Support both `gsd` 2 and 3
- Fix `gsd` requirements
- Fix `LAMMPS` backend when using charges and other cases
- Fix `Simulation` behavior when steps is a float
- Fix case of callbacks being callables
- Fix `report_stats()` argument
- Fix regression in `RUMD.__init__()`
- Fix `show()` arguments
- Fix reports
- Fix supported python versions
- Fix `f2py-jit` requirements

## 3.21.0 - 2024/03/14

[Full diff](https://framagit.org/atooms/atooms/-/compare/3.20.1...3.21.0)

###  New
- Add new module `simulation.reports`
- Add `core.utils.clear_logging()` to remove loggers
- Add `System.fix_center_of_mass()` to fix general CM properties
- Add support for targeting a function receiving `Simulation` instance

###  Bug fixes
- Fix rendering cell in `show()` with ovito backend
- Fix `trajectory.decorators.center()` by setting `cell.center` to zero
- Fix `Particle.nearest_image_of()` not returning the correct periodic image
- Fix `VerletList` initialization issues at very low density

## 3.20.1 - 2024/01/26

[Full diff](https://framagit.org/atooms/atooms/-/compare/3.20.0...3.20.1)

###  Bug fixes
- Fix behavior of `System.view()` when changing `order` argument in successive calls

## 3.20.0 - 2024/01/25

[Full diff](https://framagit.org/atooms/atooms/-/compare/3.19.2...3.20.0)

###  New
- Add `cm velocity` field to `store()` and `write_thermo()` callbacks
- Add `System.center_of_mass()` to compute center-of-mass properties efficiently
- Add `SANN` neighbors calculation
- Add (squared) distances to neighbor list
- Add `Particle.nearest_image_of()` method
- Add `full` option to neighbor list in `f90` backend
- Add dict-like behavior to System to qucikly access viewable properties, like `system['position']`

###  Bug fixes
- Optimize reading of compressed trajectories like `TrajectoryRUMD`
- Optimize `System.fix_momentum()`
- Fix Verlet lists with variable cell and/or variable particle number

## 3.19.0 - 2023/07/30

[Full diff](https://framagit.org/atooms/atooms/-/compare/3.18.0...3.19.0)

###  New
- Add new cutoffs and potentials in `backend.f90` (from database). Setting cutoff parameters occurs directly in cutoff.init(), which fixes a long standing bug with neighbors list and some cutoffs.

## 3.18.0 - 2023/07/27

[Full diff](https://framagit.org/atooms/atooms/-/compare/3.17.1...3.18.0)

###  New
- Add `System.get()` as alias of `System.dump()`
- Add `TrajectoryRamLight` to store systems in RAM with a lower memory footprint. This could become the default `TrajectoryRam` in future releases.
  
###  Bug fixes
- Fix `Simulation.wall_time()` while not running and when restarting
- Fix `f90.interaction.Interaction` with polydisperse particles and neighbor list
- Ensure F-order of arrays returned by `System.dump()` and `System.view()`

## 3.17.1 - 2023/06/15

[Full diff](https://framagit.org/atooms/atooms/-/compare/3.17.0...3.17.1)

###  Bug fixes
- Fix `System.view()` not working with cell properties. This is a crucial bug fix for NPT simulations.
- Fix import of `backends.f90.trajectory`

## 3.17.0 - 2023/06/04

[Full diff](https://framagit.org/atooms/atooms/-/compare/3.16.0...3.17.0)

###  New
- Add native `backends.f90` to compute interactions, adapted from `atooms-models`

## 3.16.0 - 2023/05/06

[Full diff](https://framagit.org/atooms/atooms/-/compare/3.15.0...3.16.0)

###  New
- Add support to charge atom_style in `trajectory.lammps`
- Add `path` parameter to `atooms.trajectory.utils.split()` to control output path (defaults to `'{base}-{step:09}{ext}'`)
- Add `output` parameter to `clockit()` and `Timer()` (defaults to `sys.stdout`)

###  Bug fixes
- Fix initialization of `particle.velocity` with dimensionality other than 3. This fixes the issue that `Particle(position=[0., 0.])` would create a particle with 2d position and 3d velocity.
- Do not fiddle with `logging.NullHandler`

## 3.15.0 - 2023/03/11

[Full diff](https://framagit.org/atooms/atooms/-/compare/3.14.2...3.15.0)

###  New
- Add `data` parameter to `TrajectoryRam` constructor. This allows one to load trajectory from a data dictionary with keys matching the `what` parameters of `System.view()` and entries in the form of lists or arrays of len equal to the number of frames.

###  Bug fixes
- Fix `distinct_species()` behavior when species is a numpy scalar. Before this fix, `distinct_species()` converted numpy integer scalar species to integers and failed with numpy string scalars.

## 3.14.2 - 2023/01/06

[Full diff](https://framagit.org/atooms/atooms/-/compare/3.14.1...3.14.2)

###  Bug fixes
- Accept db argument in store() for backward compatibility

## 3.14.0 - 2022/12/09

[Full diff](https://framagit.org/atooms/atooms/-/compare/3.13.1...3.14.0)

### Added
- Allow simulation data to be stored in instance as data dict via `store` observer
- Allow indexing of attributes in what arguments of `store` and `write` observers

### Bug fixes
- Fix `store()`

## 3.13.1 - 2022/12/08

[Full diff](https://framagit.org/atooms/atooms/-/compare/3.13.0...3.13.1)

### Bug fixes
- Fix simulation.observers.store()

## 3.11.0 - 2022/24/24

[Full diff](https://framagit.org/atooms/atooms/-/compare/3.10.0...3.11.0)

### Added
- Add System.view()

## 3.10.0 - 2022/09/28

[Full diff](https://framagit.org/atooms/atooms/-/compare/3.9.0...3.10.0)

### Added
- Add parsing of energies in lammps file

## 3.8.0 - 2022/04/29

[Full diff](https://framagit.org/atooms/atooms/-/compare/3.7.0...3.8.0)

### Added
- Support custom checkpoint variables in Simulation subclasses

## 3.7.0 - 2022/01/31

[Full diff](https://framagit.org/atooms/atooms/-/compare/3.6.0...3.7.0)

### New features
- Add TrajectoryDynamO (read only at the moment)

### Bug fixes
- Fix Simulation behavior with active progress bar (regression from f188bdab)
- Fix writing species in hdf5 trajectory when they are not stored as strings
- Fix reading SuperTrajectory with unsortable file names
- SuperTrajectory now propagates variables
- Particle should not change the datatype of position and velocity
- TrajectoryHDF5 provides float64 arrays for position and velocity by default

## 3.6.0 - 2022/01/18

[Full diff](https://framagit.org/atooms/atooms/-/compare/3.5.0...3.6.0)

### New features
- Add `radius` parameter and coloring support in `visualize.show_ovito()`
- Add `N` and `d` parameters in `System.__init__()` to initialize the system
- Add `System.composition` and `System.concentration`
- Add `position_unfolded` alias in `System.dump()`

### Bug fixes
- Fix simulation callback behavior when adding the same callback with different arguments
- Fix `System.dump()` behavior with successive copies/views and refactor it
- Fix `Particle.composition()` with unhashable species such as numpy 0-arrays
- Remove dead modules from pypi version

### Backward-compatible changes
- Ensure `Particle.position` and `Particle.velocity` are `float64` arrays. This fixes the behavior of hdf5 trajectories, which store float32 coordinates, and were not compatible with some backends. This change is backward compatible.

## 3.5.0 - 2022/01/05

[Full diff](https://framagit.org/atooms/atooms/-/compare/3.4.3...3.5.0)

### New features
- Add system.cm() to compute general attribute of CM

### Backward-compatible changes
- Remove `particle` from attributes in comment header of xyz files. This change is backward compatible.

## 3.4.0 - 2021/11/18

[Full diff](https://framagit.org/atooms/atooms/-/compare/3.3.4...3.4.0)

### New features
- Refactor and improve simulation observers
  - Refactor `write_thermo()` and `write_config()` as stateless functions
  - Add optional `trajectory_class` parameter to `write_trajectory()` to choose the trajectory format
  - Add optional `trajectory` parameter to `write_trajectory()` to write to an existing `Trajectory`
  - Add `store()` simulation observer to store properties in a dict during the simulation
  - Add option `variables` to `write_trajectory()` simulation observe
  - Support more general attributes writing in `write()` via the `what` variable
  - Expect deprecations for `write_config()` (use `write_trajectory` instead)
  - Expect deprecations for `write_to_ram()` (use `write_trajectory` passing a `TrajectoryRAM` instance instead)
- Add default value for `Simulation.trajectory_class` as `TrajectoryXYZ`
- Add `coordinate` and `momentum` arrays to reservoirs to enable in-place modification (f2py) and chains
- Reservoir masses are arrays too
	 
## 3.3.0 - 2021/11/02

[Full diff](https://framagit.org/atooms/atooms/-/compare/3.2.0...3.3.0)

### New features

- Add `Wall` class and optional instances as `System.wall`
- Add `System.species_layout` property to show and change the chemical species layout (A, C, F)
- Add `InteractionBase` as the base for actual Interaction subclasses
- Refactor `Interaction` to handle multiple interaction terms via the `term` list variable

## 3.2.0 - 2021/10/31

[Full diff](https://framagit.org/atooms/atooms/-/compare/3.1.0...3.2.0)

#### New features

- Add syntax to optionally specify the data type of Interaction.variables as <property[:dtype]>. This allows dumping arrays with the correct data type.

## 3.1.0 - 2021/10/30

[Full diff](https://framagit.org/atooms/atooms/-/compare/3.0.0...3.1.0)

### New features

- Add `TrajectoryFactory.register_callback()`
- Argument `what` in `System.compute_interaction()` is now optional

## 3.0.0 - 2021/10/03

[Full diff](https://framagit.org/atooms/atooms/-/compare/2.8.1...3.0.0)

### Changes
- Interaction class now belongs to system.interaction module
- Change the interface of Interaction.compute() method so as to accept System arbitrary attributes. Thanks to this change, Interaction now behaves as a strategy pattern for System
- Use new Trajectory.copy() method to convert trajectory, instead of trajectory.utils.convert()
- Rename inappropriate Trajectory.trajectory attribute in Trajectory subclasses. The new convention is to use _file as private attribute for the file object
- Rename index attribute to frame in TrajectoryBase.read_system()
- Change options order in Particle constructor
- Change system.distinct_species() method as a property
- RUMD backend requires version 3
- Rename forcefield option as potentials in RUMD backend
- Merge autopep8 and flake8 Makefile targets into pep8

### Removals
- Remove Interaction subpackage, including potential tabulation stuff, which will be moved to a specific backend.
- Remove atooms.backends.f90, it will be added to the atooms-models package
- Remove Trajectory.self_callbacks and Trajectory.class_callbacks
- Remove atooms.simulation.umbrella
- Remove system.particle.show() function, because it belongs to System
- Remove system.particle.show_*() functions, because they belong to visualize
- Remove trajectory.utils.convert() and trajectory.utils.modify_fields()
- Remove unfolded parameter from TrajectoryHDF5.read_sample()
- Remove support for cell values as last line in xyz trajectory format
- Remove support for matrix attribute in System
- Remove support for list of attributes passed to System.dump(), use dict compreheshion instead to collect dumped attributes
- Remove report() methods throughout, rely on __str__() instead
- Remove forcefield_file option from RUMD backend, pass RUMD Potentials instances instead
- Remove user and develop targets from Makefile
- Remove tox.ini

### Deprecations
- Trajectory.write_sample() in favor of write_system()
- Trajectory.read_sample() in favor of read_system()
- Trajectory.fields in favor of Trajectory.variables

### New features
- Add Trajectory.variables to store System attributes that change along the trajectory
- Add core.utils.canonicalize() as standalone function
- Add contribution guidelines
- Add setup.cfg configuration file


